window.contentLoaded(window, function (e) {
	let dts = document.querySelectorAll(".accordion dt");

	dts.forEach((dt, indexOuter) => {
		dt.addEventListener("click", (e) => {
			dts.forEach((el, indexInner) => {
				if (indexOuter != indexInner) {
					el.classList.remove("active");
				}
			});

			dt.classList.add("active");
		});
	});
});
