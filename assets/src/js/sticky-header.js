window.contentLoaded(window, function (e) {
	window.addEventListener("scroll", () => {
		window.scrollY >= 50
			? document.querySelector("header").classList.add("scrolled")
			: document.querySelector("header").classList.remove("scrolled");
	});
});
