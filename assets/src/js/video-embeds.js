window.contentLoaded(window, function (e) {
	let videoTriggers = document.querySelectorAll(".inline-video-trigger");
	if(videoTriggers) {
		videoTriggers.forEach((trigger) => {
			trigger.addEventListener("click", () => {
				var parent = trigger.parentNode;
				if (trigger.getAttribute("data-video-id") && parent.classList.contains('video-preview-container')) {

					let iframeHTML = document.createElement("iframe");
					let id = trigger.getAttribute("data-video-id");
					let oldIframe = parent.querySelector("iframe");
	
					iframeHTML.setAttribute('frameborder', 0);
					iframeHTML.setAttribute('allow', 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture');
	
					if (trigger.classList.contains("vimeo")) {
						iframeHTML.src = `https://player.vimeo.com/video/${id}?title=0&byline=0&portrait=0?&autoplay=1`;
					} else {
						iframeHTML.src = `https://www.youtube.com/embed/${id}?autoplay=1`;
					}
	
					trigger.style.display = "none";
					parent.querySelector(".overlay").style.display = "none";

					if(oldIframe) {
						oldIframe.remove();
					}
					
					parent.appendChild(iframeHTML);
	
				} else {
					console.error("no video ID provided.");
				}
			});
		});
	}
});
